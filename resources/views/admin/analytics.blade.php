@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
        @if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
        @endif
        @if (session('error_message'))
          <div class="alert alert-danger">{{ session('error_message') }}</div>
        @endif
      </div>
    </div>
	
  <div class="row">
    <form method="post">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="col-xs-4 col-xs-offset-1">
        <select name="statistic" id="statistictype" class="form-control">
          <option value="">-- Select Statistic --</option>
          <option value="relation" {{ isset($type) && $type == 'all' ? 'selected' : '' }}>Relation in Front of Each Other</option>
          <option value="perdate" {{ isset($type) && $type == 'perdate' ? 'selected' : '' }}>Clicks per date</option>
        </select>
      </div>
	  
      <div class="col-xs-2 col-xs-offset-1">
        <select name="link" id="linktype" class="form-control">
          <option value="">All Links</option>
		  @foreach($clicks as $click)
          	<option value="{{$click->title}}" {{isset($linktype) && $linktype == $click->title ? 'selected' : '' }}>{{$click->title}}</option>
          @endforeach
	  </select>
      </div>

      <div class="col-xs-2 col-xs-offset-1">
        <input type="submit" class="btn btn-primary" value="Analyze">
      </div>
    </form>
  </div>

  <hr>

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
		@if(!isset($type) || $type == 'relation')
			<div class="panel-heading">Relation in Front of Each Other</div>
		@else
			<div class="panel-heading">Clics per Date</div>
		@endif
		
	    <div class="panel-body">

			@if(!isset($type) || $type == 'relation' )
			<div class="row">
				<div class="col-md-6 col-xs-12">
					<p>Clicks</p>
					<table>
						<tr>
							<th>Title</th>
							<th>Number of clicks</th>
						</tr>
						<?php $num = 1;?>
						@foreach($clicks as $click)
							<tr>
								<td>{{$num++}}. {{$click->title}}</td>
								<td>{{$click->count}} times</td>
							</tr>
						@endforeach
					</table>
				</div>
				<div class="col-md-6 col-xs-12">
					<div id="search-pie"></div>
				</div>
			</div>
			@endif
			
			@if(isset($type) && $type == 'perdate')
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<p>Clics per Date</p>
					
					This is not done. If you like me to do it, please let me know.
				</div>
			</div>
			@endif
			
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection


@section('scripts')
	<script src="{{ asset('js/script.js') }}"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	
	@if(!isset($type) || $type == 'relation')
	<script type="text/javascript">
		google.charts.load("current", {packages:["corechart"]});
		google.charts.setOnLoadCallback(drawChart);
		function drawChart() {
			var data = google.visualization.arrayToDataTable([
				['Title', 'Number of clicks'],
				@foreach($clicks as $click)
					['{{$click->title}}', {{ $click->count }}],
				@endforeach
			]);

			var options = {
      		  title: 'Analitycs of All Clicks',
      		  pieHole: 0.3,
    		};

			var chart = new google.visualization.PieChart(document.getElementById('search-pie'));
			chart.draw(data, options);
		}
	</script>
	@endif
	
	
@endsection