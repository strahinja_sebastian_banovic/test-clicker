<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

class AdminController extends Controller
{
	public function analytics()
	{
		//TODO Ovde ide ispitivanje i redirekt na login ako nije admin
	
		$clicks = DB::table('clicks')
			->select(DB::raw('count(*) as count, clicks.title'))
			->groupBy('title')
			->get();
		return view('admin.analytics', compact('clicks'));
	}
}
