<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

class SiteController extends Controller
{
    public function index()
	{
		return view('welcome');
	}
	
	
	/**
	 *  Ovaj deo, kao i u ostalie funkcije istog tipa je mogao da se uradi sa      
	 * updateovanjem jednog dodatnog polja sa counterom koliko je puta kliknuto na 
	 * taj link. U sustini taj deo u ovom slucaju ne predstavlja razliku, ali ima  
	 * slucajeva u zavisnosti od sitea i u slucaju dodavanja linkova u adminu kada bi 
	 * bilo pametno staviti tu dodatnu kolonu. Naravno da MySQL ima najmanje 
	 * kapacitete od baza, ali opet kazem da je ovo mali test.
	 */
	public function documentation()
	{
		DB::table('clicks')->insert([
			'title' => 'documentation'
				]);
		return redirect('https://laravel.com/docs');
	}
	
	public function laracast()
	{
		DB::table('clicks')->insert([
			'title' => 'laracast'
				]);
		return redirect('https://laracasts.com');
	}
	
	public function news()
	{
		DB::table('clicks')->insert([
			'title' => 'news'
				]);
		return redirect('https://laravel-news.com');
	}
	
	public function forge()
	{
		DB::table('clicks')->insert([
			'title' => 'forge'
				]);
		return redirect('https://forge.laravel.com');
	}
	
	public function github()
	{
		DB::table('clicks')->insert([
			'title' => 'github'
				]);
		return redirect('https://github.com/laravel/laravel');
	}
}
