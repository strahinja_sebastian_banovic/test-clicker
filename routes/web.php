<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'SiteController@index');
Route::get('admin', 'AdminController@analytics');
Route::get('documentation', 'SiteController@documentation');
Route::get('laracast', 'SiteController@laracast');
Route::get('news', 'SiteController@news');
Route::get('forge', 'SiteController@forge');
Route::get('github', 'SiteController@github');
