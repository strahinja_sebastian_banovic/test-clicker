$(document).ready(function(){

	// Statistics
	if($('#statistictype').val() != 'relation' &&  $('#statistictype').val() != '') {
		$('#linktype').show()
	} else {
		$('#linktype').hide()
	}
	$(document).on('change', '#statistictype', function(e){
		e.preventDefault();
		if($(this).val() != 'relation' &&  $(this).val() != '') {
			$('#linktype').show()
		} else {
			$('#linktype').hide()
		}
	});

});